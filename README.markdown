# Create and style clusters

\_A Pen created at CodePen.io. Original URL: [https://codepen.io/jashan510/pen/qBdKjPQ](https://codepen.io/jashan510/pen/qBdKjPQ).

Use Mapbox GL JS' built-in functions to visualize points as clusters.

See the example: [https://docs.mapbox.com//mapbox-gl-js/example/cluster/](https://docs.mapbox.com//mapbox-gl-js/example/cluster/)

Open scr/index.html to view example
We will be using two types of mapbox layers

1. Heatmap - to show tracks of infected areas
2. Clustering - to show cluster of locations where infected people (covid-19 positive) are present at the moment
